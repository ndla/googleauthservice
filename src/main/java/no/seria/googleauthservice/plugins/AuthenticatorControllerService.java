package no.seria.googleauthservice.plugins;

import no.seria.googleauthservice.service.session.ExternalAuthSession;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface AuthenticatorControllerService {
    Object startAuthentication(HttpServletResponse response, ExternalAuthSession session) throws IOException;
}
