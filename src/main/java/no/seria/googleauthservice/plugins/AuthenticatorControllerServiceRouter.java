package no.seria.googleauthservice.plugins;

import java.util.Map;

public class AuthenticatorControllerServiceRouter {
    private Map<String,AuthenticatorControllerService> handlers;

    public AuthenticatorControllerServiceRouter(Map<String,AuthenticatorControllerService> handlers) {
        this.handlers = handlers;
    }

    public AuthenticatorControllerService getAuthenticatorControllerService(String name) {
        synchronized (handlers) {
            return handlers.get(name);
        }
    }
}
