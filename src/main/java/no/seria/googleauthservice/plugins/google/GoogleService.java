package no.seria.googleauthservice.plugins.google;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.AuthorizationCodeTokenRequest;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import no.seria.googleauthservice.data.webclient.WebClient;
import no.seria.googleauthservice.plugins.AuthenticatorControllerService;
import no.seria.googleauthservice.data.domain.ExternalUserData;
import no.seria.googleauthservice.service.ExternalAuthFacadeService;
import no.seria.googleauthservice.service.session.ExternalAuthSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

public class GoogleService implements AuthenticatorControllerService {
    @Autowired
    private WebClient webClient;
    @Autowired
    private ExternalAuthFacadeService externalAuthFacadeService;
    private GoogleConfiguration configuration;

    public GoogleService(GoogleConfiguration configuration) {
        this.configuration = configuration;
    }
    private Collection<String> getScopes() {
        Collection<String> scopes = new HashSet<String>();
        scopes.add("openid");
        scopes.add("profile");
        scopes.add("email");
        return scopes;
    }
    private AuthorizationCodeFlow getAuthFlow(Collection<String> scopes) {
        AuthorizationCodeFlow authFlow = new GoogleAuthorizationCodeFlow.Builder(new NetHttpTransport(), new JacksonFactory(), configuration.getAppId(), configuration.getAppKey(), scopes).build();
        return authFlow;
    }
    private URI getReturnUri() throws IOException {
        return ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(GoogleController.class).returnEndpoint(null, null)).toUri();
    }
    @Override
    public Object startAuthentication(HttpServletResponse response, ExternalAuthSession session) throws IOException {
        AuthorizationCodeFlow authFlow = getAuthFlow(getScopes());
        AuthorizationCodeRequestUrl requestUrl = authFlow.newAuthorizationUrl();
        requestUrl.setRedirectUri(getReturnUri().toString());
        requestUrl.setState(session.getId());
        response.sendRedirect(requestUrl.toString());
        return null;
    }
    public Object handleReturn(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String,String[]> params = request.getParameterMap();
        ExternalAuthSession session;
        if (params.containsKey("state")) {
            String[] states = params.get("state");
            if (states.length > 0) {
                String state = states[0];
                session = externalAuthFacadeService.getSession(state);
                if (session == null) {
                    System.out.println("Fatal(HTTP 400): The state field is invalid or expired");
                    response.sendError(400);
                    return null;
                }
            } else {
                System.out.println("Fatal(HTTP 400): Missing state field in return from Google");
                response.sendError(400);
                return null;
            }
        } else {
            System.out.println("Fatal(HTTP 400): Missing state field in return from Google");
            response.sendError(400);
            return null;
        }
        if (params.containsKey("code")) {
            String[] codes = params.get("code");
            if (codes.length > 0) {
                String code = codes[0];
                AuthorizationCodeFlow authFlow = getAuthFlow(getScopes());
                AuthorizationCodeTokenRequest req = authFlow.newTokenRequest(code);
                req.setRedirectUri(getReturnUri().toString());
                TokenResponse tokenResponse = req.execute();
                URL url;
                try {
                    url = new URL("https://www.googleapis.com/plus/v1/people/me");
                } catch (MalformedURLException e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
                url = signUrl(url, tokenResponse);
                JsonObject json = webClient.fetchJsonObject(url);
                GoogleUserDataImpl userData = new GoogleUserDataImpl(json);
                session.setExternalUserData(userData.getExternalUserData());
                URI returnUri = session.getReturnUri();
                response.sendRedirect(returnUri.toString());
                return null;
            } else {
                System.out.println("Fatal(HTTP 400): Missing code field in return from Google");
                response.sendError(400);
                return null;
            }
        } else {
            System.out.println("Fatal(HTTP 400): Missing code field in return from Google");
            response.sendError(400);
            return null;
        }
    }

    public URL signUrl(URL api, String accessToken) {
        String apiStr = api.toString();
        String delim;
        if (apiStr.indexOf('?') >= 0)
            delim = "&";
        else
            delim = "?";
        try {
            return new URL(apiStr+delim+"access_token="+accessToken);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    public URL signUrl(URL api, TokenResponse tokenResponse) {
        return signUrl(api, tokenResponse.getAccessToken());
    }

    private class GoogleUserDataImpl {
        private String id;
        private Collection<String> emails;
        private String firstName;
        private String lastName;

        public GoogleUserDataImpl(JsonObject userDataObject) {
            emails = new ArrayList<String>();
            JsonArray emailsJsonArray = userDataObject.getJsonArray("emails");
            for (JsonValue value : emailsJsonArray) {
                if (value instanceof JsonObject) {
                    JsonObject emailJsonObject = (JsonObject) value;
                    String email = emailJsonObject.getJsonString("value").getString();
                    emails.add(email);
                }
            }
            id = userDataObject.getJsonString("id").getString();
            JsonObject nameJsonObject = userDataObject.getJsonObject("name");
            firstName = nameJsonObject.getJsonString("givenName").getString();
            lastName = nameJsonObject.getJsonString("familyName").getString();
        }

        public ExternalUserData getExternalUserData() {
            return new ExternalUserData() {
                @Override
                public String getId() {
                    return id;
                }

                @Override
                public Collection<String> getEmails() {
                    return emails;
                }

                @Override
                public String getDisplayName() {
                    return firstName + " " + lastName;
                }

                @Override
                public String getFirstName() {
                    return firstName;
                }

                @Override
                public String getLastName() {
                    return lastName;
                }
            };
        }

        public String getId() {
            return id;
        }

        public Collection<String> getEmails() {
            Collection<String> cl = new ArrayList<String>(emails.size());
            cl.addAll(emails);
            return cl;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }
    }
}
