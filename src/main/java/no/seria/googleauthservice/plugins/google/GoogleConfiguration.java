package no.seria.googleauthservice.plugins.google;

public class GoogleConfiguration {
    private String appId;
    private String appKey;

    public GoogleConfiguration(String appId, String appKey) {
        this.appId = appId;
        this.appKey = appKey;
    }

    public String getAppId() {
        return appId;
    }

    public String getAppKey() {
        return appKey;
    }
}
