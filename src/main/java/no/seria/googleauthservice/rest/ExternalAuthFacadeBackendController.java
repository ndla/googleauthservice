package no.seria.googleauthservice.rest;

import no.seria.googleauthservice.data.domain.ExternalAuthData;
import no.seria.googleauthservice.data.domain.ExternalUserData;
import no.seria.googleauthservice.service.ExternalAuthFacadeService;
import no.seria.googleauthservice.service.session.ExternalAuthSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

@Controller
@RequestMapping("/backend/googleauth")
public class ExternalAuthFacadeBackendController {
    @Autowired
    private ExternalAuthFacadeService externalAuthFacadeService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public Object createAuthSession(HttpServletResponse response, @RequestParam(value = "return", required = true) String returnUri) throws IOException {
        ExternalAuthSession session = externalAuthFacadeService.createSession();
        try {
            session.setReturnUri(new URI(returnUri));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            response.sendError(400);
            return null;
        }
        JSONObject createResponse = new JSONObject();
        createResponse.put("id", session.getId());
        synchronized (Date.class) {
            createResponse.put("expiry", session.getExpiry().toString());
        }
        createResponse.put("state", ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(getClass()).getData(null, session.getId())).toUri().toString());
        createResponse.put("action", ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(ExternalAuthFacadeFrontendController.class).startAuthentication(null, session.getId())).toUri().toString());
        String responseBody = createResponse.toString();
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();
        writer.print(responseBody);
        writer.close();
        return null;
    }
    @RequestMapping(value = "/state/{id}", method = RequestMethod.GET)
    public Object getData(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        ExternalAuthSession session = externalAuthFacadeService.getSession(id);
        if (session == null) {
            response.sendError(404);
            return null;
        }
        JSONObject dataResponse = new JSONObject();
        dataResponse.put("url", ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(getClass()).getData(null, session.getId())).toUri().toString());
        JSONObject data = new JSONObject();
        data.put("id", session.getId());
        synchronized (Date.class) {
            data.put("expiry", session.getExpiry().toString());
        }
        ExternalUserData externalAuthData = session.getExternalUserData();
        if (externalAuthData != null) {
            JSONObject userData = new JSONObject();
            userData.put("id", externalAuthData.getId());
            userData.put("firstName", externalAuthData.getFirstName());
            userData.put("lastName", externalAuthData.getLastName());
            userData.put("displayName", externalAuthData.getDisplayName());
            JSONArray emailArray = new JSONArray();
            for (String email : externalAuthData.getEmails()) {
                emailArray.put(email);
            }
            userData.put("email", emailArray);
            data.put("data", userData);
        }
        dataResponse.put("data", data);
        String responseBody = dataResponse.toString();
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();
        writer.print(responseBody);
        writer.close();
        return null;
    }
    @RequestMapping(value = "/state/{id}", method = RequestMethod.DELETE)
    public Object deleteAuthSession(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        ExternalAuthSession session = externalAuthFacadeService.getSession(id);
        if (session == null) {
            response.sendError(404);
            return null;
        }
        externalAuthFacadeService.deleteSession(id);
        JSONObject responseObj = new JSONObject();
        responseObj.put("id", id);
        responseObj.put("result", "deleted");
        String responseBody = responseObj.toString();
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();
        writer.print(responseBody);
        writer.close();
        return null;
    }
}
