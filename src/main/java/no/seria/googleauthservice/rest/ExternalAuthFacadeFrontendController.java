package no.seria.googleauthservice.rest;

import no.seria.googleauthservice.plugins.AuthenticatorControllerServiceRouter;
import no.seria.googleauthservice.service.ExternalAuthFacadeService;
import no.seria.googleauthservice.service.session.ExternalAuthSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/authservice/googleauth")
public class ExternalAuthFacadeFrontendController {
    @Autowired
    private ExternalAuthFacadeService externalAuthFacadeService;
    @Autowired
    private AuthenticatorControllerServiceRouter authenticatorControllerServiceRouter;

    @RequestMapping("/start/{id}")
    public Object startAuthentication(HttpServletResponse response, @PathVariable("id") String id) throws IOException {
        ExternalAuthSession session = externalAuthFacadeService.getSession(id);
        if (session != null) {
            return authenticatorControllerServiceRouter.getAuthenticatorControllerService("googleauth").startAuthentication(response, session);
        } else {
            response.sendError(404);
            return null;
        }
    }
}
