package no.seria.googleauthservice.service.session;

import no.seria.googleauthservice.data.domain.ExternalAuthData;

/**
 * Facade interface for the auth data object.
 */
public interface ExternalAuthSession extends ExternalAuthData {
}
