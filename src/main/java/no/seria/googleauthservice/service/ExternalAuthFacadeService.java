package no.seria.googleauthservice.service;

import no.seria.googleauthservice.data.ExternalAuthDao;
import no.seria.googleauthservice.data.domain.ExternalAuthData;
import no.seria.googleauthservice.data.domain.ExternalUserData;
import no.seria.googleauthservice.service.session.ExternalAuthSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Date;

@Component
public class ExternalAuthFacadeService {
    @Autowired
    private ExternalAuthDao externalAuthDao;

    private class ExternalAuthSessionImpl implements  ExternalAuthSession {
        private ExternalAuthData dataObject;

        public ExternalAuthSessionImpl(ExternalAuthData dataObject) {
            this.dataObject = dataObject;
        }

        @Override
        public String getId() {
            return dataObject.getId();
        }

        @Override
        public void setExpiry(Date expiry) {
            dataObject.setExpiry(expiry);
        }

        @Override
        public Date getExpiry() {
            return dataObject.getExpiry();
        }

        @Override
        public void setReturnUri(URI returnUri) {
            dataObject.setReturnUri(returnUri);
        }

        @Override
        public URI getReturnUri() {
            return dataObject.getReturnUri();
        }

        @Override
        public void setExternalUserData(ExternalUserData userData) {
            dataObject.setExternalUserData(userData);
        }

        @Override
        public ExternalUserData getExternalUserData() {
            return dataObject.getExternalUserData();
        }
    }
    public ExternalAuthSession createSession() {
        ExternalAuthData externalAuthData = externalAuthDao.createExternalAuthData();
        Date now, expiry;
        synchronized (Date.class) {
            now = new Date();
            expiry = new Date(now.getTime() + 36000000);
        }
        externalAuthData.setExpiry(expiry);
        return new ExternalAuthSessionImpl(externalAuthData);
    }
    public ExternalAuthSession getSession(String id) {
        ExternalAuthData externalAuthData = externalAuthDao.getExternalAuthData(id);
        if (externalAuthData != null) {
            return new ExternalAuthSessionImpl(externalAuthData);
        } else {
            return null;
        }
    }
    public void deleteSession(String id) {
        ExternalAuthData externalAuthData = externalAuthDao.getExternalAuthData(id);
        if (externalAuthData != null) {
            externalAuthDao.deleteExternalAuthData(externalAuthData);
        }
    }
}
