package no.seria.googleauthservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@EnableAutoConfiguration
@ImportResource("classpath*:googleauthservice.xml")
public class GoogleauthserviceApp {
    public static void main(String[] args) {
        SpringApplication.run(GoogleauthserviceApp.class, args);
    }
}
