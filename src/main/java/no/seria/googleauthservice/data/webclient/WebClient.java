package no.seria.googleauthservice.data.webclient;

import org.apache.http.client.methods.HttpUriRequest;

import javax.json.JsonObject;
import java.io.IOException;
import java.net.URL;

public interface WebClient {
    public RequestFilter filter();
    public JsonObject fetchJsonObject(URL url) throws IOException;

    interface RequestFilter {
        public RequestFilter basicAuth(String username, String password);
        public void filter(HttpUriRequest req);
        public JsonObject fetchJsonObject(URL url) throws IOException;
    }
}
