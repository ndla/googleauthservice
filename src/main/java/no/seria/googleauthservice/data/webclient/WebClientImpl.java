package no.seria.googleauthservice.data.webclient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClients;
import sun.misc.BASE64Encoder;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;

public class WebClientImpl implements WebClient {
    private HttpClient httpClient;

    public WebClientImpl() {
        this.httpClient = HttpClients.createDefault();
    }

    private ResponseHandler<JsonObject> getJsonResponseHandler() {
        return new ResponseHandler<JsonObject>() {
            @Override
            public JsonObject handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                StatusLine statusLine = response.getStatusLine();
                HttpEntity entity = response.getEntity();
                if (statusLine.getStatusCode() >= 300) {
                    InputStream content = entity.getContent();
                    byte[] fullContent = null;
                    while (true) {
                        byte[] contentBuffer = new byte[8192];
                        int rd = content.read(contentBuffer);
                        if (rd <= 0)
                            break;
                        if (rd < contentBuffer.length) {
                            byte[] tmp = new byte[rd];
                            System.arraycopy(contentBuffer, 0, tmp, 0, rd);
                            contentBuffer = tmp;
                        }
                        if (fullContent == null) {
                            fullContent = contentBuffer;
                        } else {
                            byte[] tmp = new byte[fullContent.length + contentBuffer.length];
                            System.arraycopy(fullContent, 0, tmp, 0, fullContent.length);
                            System.arraycopy(contentBuffer, 0, tmp, fullContent.length, contentBuffer.length);
                            fullContent = tmp;
                        }
                    }
                    String errorContent = new String(fullContent, "UTF-8");
                    System.out.println("Error content from google: "+errorContent);
                    throw new HttpResponseException(
                            statusLine.getStatusCode(),
                            statusLine.getReasonPhrase());
                }
                if (entity == null) {
                    throw new ClientProtocolException("Response contains no content");
                }
                ContentType contentType = ContentType.getOrDefault(entity);
                Charset charset = contentType.getCharset();
                JsonReader jsonReader = Json.createReader(entity.getContent());
                return jsonReader.readObject();
            }
        };
    }
    public JsonObject requestJson(HttpUriRequest request) throws ClientProtocolException, IOException {
        return httpClient.execute(request, getJsonResponseHandler());
    }
    private static class RequestFilterImpl implements RequestFilter {
        private WebClientImpl webClient;
        private String basicUsername = null;
        private String basicPassword = null;

        public RequestFilterImpl(WebClientImpl webClient) {
            this.webClient = webClient;
        }
        @Override
        public RequestFilter basicAuth(String username, String password) {
            this.basicUsername = username;
            this.basicPassword = password;
            return this;
        }

        @Override
        public void filter(HttpUriRequest req) {
            if (basicPassword != null && basicUsername != null) {
                String authentication = basicUsername+":"+basicPassword;
                String encoded = new BASE64Encoder().encode(authentication.getBytes());
                req.addHeader("Authorization", "Basic "+encoded);
            }
        }

        @Override
        public JsonObject fetchJsonObject(URL url) throws IOException {
            return webClient.fetchJsonObject(this, url);
        }
    }
    public JsonObject fetchJsonObject(RequestFilter filter, URL url) throws IOException {
        HttpGet get = new HttpGet(url.toString());
        filter.filter(get);
        return requestJson(get);
    }

    @Override
    public RequestFilter filter() {
        return new RequestFilterImpl(this);
    }

    @Override
    public JsonObject fetchJsonObject(URL url) throws ClientProtocolException, IOException {
        return filter().fetchJsonObject(url);
    }
}
