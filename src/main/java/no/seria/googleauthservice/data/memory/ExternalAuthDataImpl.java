package no.seria.googleauthservice.data.memory;

import no.seria.googleauthservice.data.domain.ExternalAuthData;
import no.seria.googleauthservice.data.domain.ExternalUserData;

import java.net.URI;
import java.util.Date;

public class ExternalAuthDataImpl implements ExternalAuthData {
    private String id;
    private Date expiry;
    private URI returnUri;
    private ExternalUserData externalUserData;

    public void setId(String id) {
        this.id = id;
    }
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    @Override
    public Date getExpiry() {
        return expiry;
    }

    @Override
    public URI getReturnUri() {
        return returnUri;
    }

    @Override
    public void setExternalUserData(ExternalUserData userData) {
        externalUserData = userData;
    }

    @Override
    public ExternalUserData getExternalUserData() {
        return externalUserData;
    }

    @Override
    public void setReturnUri(URI returnUri) {
        this.returnUri = returnUri;
    }
}
