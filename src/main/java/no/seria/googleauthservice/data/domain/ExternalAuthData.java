package no.seria.googleauthservice.data.domain;

import java.net.URI;
import java.util.Date;

public interface ExternalAuthData {
    String getId();
    void setExpiry(Date expiry);
    Date getExpiry();
    void setReturnUri(URI returnUri);
    URI getReturnUri();
    void setExternalUserData(ExternalUserData userData);
    ExternalUserData getExternalUserData();
}
