package no.seria.googleauthservice.data.domain;

import java.util.Collection;

public interface ExternalUserData {
    public String getId();
    public Collection<String> getEmails();
    public String getDisplayName();
    public String getFirstName();
    public String getLastName();
}
