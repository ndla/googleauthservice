package no.seria.googleauthservice.data;

import no.seria.googleauthservice.data.domain.ExternalAuthData;

public interface ExternalAuthDao {
    ExternalAuthData createExternalAuthData();
    ExternalAuthData getExternalAuthData(String id);
    void deleteExternalAuthData(ExternalAuthData externalAuthData);
}
