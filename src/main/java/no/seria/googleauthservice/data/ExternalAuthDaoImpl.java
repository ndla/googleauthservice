package no.seria.googleauthservice.data;

import no.seria.googleauthservice.data.domain.ExternalAuthData;
import no.seria.googleauthservice.data.memory.ExternalAuthDataImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;

import java.util.*;

public class ExternalAuthDaoImpl implements ExternalAuthDao {
    private Map<String,ExternalAuthData> externalAuthDataMap = new HashMap<String,ExternalAuthData>();

    @Autowired
    private TaskScheduler taskScheduler;
    private Runnable cleanup = new Runnable() {
        @Override
        public void run() {
            synchronized (externalAuthDataMap) {
                List<String> remove = new ArrayList<String>();
                for (Map.Entry<String,ExternalAuthData> entry : externalAuthDataMap.entrySet()) {
                    if (isExpired(entry.getValue())) {
                        remove.add(entry.getKey());
                    }
                }
                for (String key : remove) {
                    System.out.println("Garbage collected authentication state "+key);
                    externalAuthDataMap.remove(key);
                }
            }
        }
    };
    private boolean scheduledCleanup = false;

    private boolean isExpired(ExternalAuthData data) {
        Date expiry = data.getExpiry();
        Date now;
        long expiryTm;
        long nowTm;
        synchronized (Date.class) {
            now = new Date();
            expiryTm = expiry.getTime();
            nowTm = now.getTime();
        }
        return (expiryTm < nowTm);
    }

    @Override
    public ExternalAuthData createExternalAuthData() {
        ExternalAuthDataImpl data = new ExternalAuthDataImpl();
        data.setId(UUID.randomUUID().toString());
        synchronized (externalAuthDataMap) {
            externalAuthDataMap.put(data.getId(), data);
        }
        synchronized (this) {
            if (!scheduledCleanup) {
                scheduledCleanup = true;
                taskScheduler.scheduleAtFixedRate(cleanup, 1000000);
            }
        }
        return data;
    }

    @Override
    public ExternalAuthData getExternalAuthData(String id) {
        ExternalAuthData data;
        synchronized (externalAuthDataMap) {
            data = externalAuthDataMap.get(id);
        }
        return data;
    }

    @Override
    public void deleteExternalAuthData(ExternalAuthData externalAuthData) {
        synchronized (externalAuthDataMap) {
            externalAuthDataMap.remove(externalAuthData.getId());
        }
    }
}
